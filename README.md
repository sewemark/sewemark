# Introduction

The goal of this exercise is to implement a transaction engine for a stock exchange.
The stock exchange is connected with multiple brokers. Each broker has a dedicated queue which is used to transfer orders to the exchange. The transaction engine (matching engine) receives messages from brokers and processes them in the order of their arrival.

#  Architecture

![architecture](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/architecture.png "Architecture")

Elements in dark blue are provided by GFT. Elements in light blue are the ones that you have to deliver.
The messaging platform uses ActiveMQ (v. 5.7.0) which implements JMS 1.1.

#  Content of Bitbucket contestant repo

Every contestant gets his/her own repository, which is created by GFT by forking [the template project](https://bitbucket.org/digitalbanking/template2016).

`pom.xml` – maven build file with GFT repository and dependency to runtime environment and all related elements.

If you execute the `mvn package` command in a folder to which the archive was extracted, you will build your solution. 
If you execute the `mvn test` command in a folder to which the archive was extracted, you will build your solution and run all of your unit tests.

If you execute `mvn test –P verify`, you will start the verification process of your solution. The process consists of the following steps:

1. Compilation of sources;
2. Startup of ActiveMQ;
3. Look up of implementation of com.gft.digitalbank.exchange.Exchange interface (This is the interface that you have to implement);
4. Instantiation of the looked up class and its initialization;
5. Execution of test scenarios (there are almost 50 scenarios).

Finally, if you execute the `mvn test –P performance` task, the performance tests of your solution will be run. The steps that are executed while running performance tests are identical to those of the test task, but the number of orders placed is significantly higher.

#  What you should have

We assume that you have:

1. access to a computer with Windows, Linux or MacOS operating system
2. JDK 1.8 installed
3. access to the Internet (maven will have to download all necessary dependencies)
4. some spare time

#  What you should do

Implement the `com.gft.digitalbank.exchange.Exchange` interface that comes with the contestant package. There should be exactly one such implementation in the classpath of your solution, otherwise all verification tests will fail. The implementation class has to provide a public zero-argument constructor, otherwise its instance could will not be created (it is done via reflection) and all tests will fail as well.

The interface has the following methods:

* `void registerProcessingListener(ProcessingListener processingListener)`
Registers a processing listener. The listener has one method only – `void processingDone(SolutionResults results)`, which should be called by your implementation when all the orders have been processed. The SolutionResults instance, which your solution must provide, should contain the results of the matching algorithm.
* `void setDestinations(List<String> destinations)`
The destinations argument is a reference to thea list containing queue destination names. These queues will be used to deliver orders. 
* `void start()`
Start processing of messages.

Both methods are executed only once during the initialization phase before running each verification test. It means that there is one processing listener per verification test.

2. Connect to queues listed in the destinations parameter ofof the initialize method.
In order to obtain a javax.jms.ConnectionFactory instance, you can use the following snippet: 
```java
Context context = new InitialContext();
ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");
```
3.	Process incoming orders
4.	Call the processingDone method and pass the results
5.	Make sure that all verification tests pass successfully

#  Message types

All messages sent by brokers are of text type (javax.jms.TextMessage). Each message has a `messageType` string property which specifies the type of the message. The body (i.e. `TextMessage.getText()`) of a message contains a JSON document with details specific to the given message type.

There are four types of messages:

## Buy or sell order message (messageType = “order”)

This order places a buy or sell order in the order book of a given security.

```json
{
  "messageType": "order",
  "side": <value>,
  "id": <value>,
  "timestamp": <value>,
  "broker": <value>,
  "client": <value>,
  "product": <value>,
  "details": {
    "amount": <value>,
    "price": <value>
  }
}
```

Element name | Value | Description 
--- | --- | --- 
messageType | "ORDER" | AIt’s a copy of messageType message property 
side | “BUY” or “SELL”	| BUY - corresponds to buy order with limit (buy an issue for no more than…),<br> SELL - corresponds to sell order with limit (sell an issue for no less than…) 
id | integer value (positive) | Unique id of an order
timestamp | long value (positive) | Timestamp telling indicating when an order was sent. <br>For two orders the one with the higher timestamp is the one which was sent later. <br>This applies across all brokers.
broker | String value | Identifier of a broker that sent the message
client | String value | Identifier of a client that placed the order
product	| String value | Ticker symbol of a security to buy or sell
details	| Sub-element | Sub-element containing details related to amount and price described below
amount | integer value | Amount of shares to buy or sell
price | integer value | Price limit associated with the order

Buy order example:
```json
{
  "messageType": "order",
  "side": "BUY",
  "id": 1,
  "timestamp": 1,
  "broker": "b001",
  "client": "c001",
  "product": "abc",
  "details": {
    "amount": 10,
    "price": 1.23
  },
}
```

The above order means: client c001 wants to buy via broker b001 10 shares of abc for no more than 1.23 PLN.

Sell order example:
```json
{
  "messageType": "order",
  "side": "SELL",
  "id": 2,
  "timestamp": 2,
  "broker": "b001",
  "client": "c002",
  "product": "xyz",
  "details": {
    "amount": 15,
    "price": 10.00
  },
}
```

The above order means: client c002 wants to sell via broker b001 15 shares of xyz for no less than 10.00 PLN. 

## Modification order (messageType = “modification”)

This order modifies a buy or sell order in the order book. A bBuy or sell order that was already fully executed (all shares have been bought or sold) cannot be affected by any modification. A mModification order must be sent by the same broker that sent the related buy or sell order. 
MModification of an order affects the order book sequence, that is, the modified order should be processed just only as an entirely new order.

```json
{
  "messageType": "modification",
  "id": <value>,
  "timestamp": <value>,
  "broker": <value>,
  "modifiedOrderId": <value>,
  "details": {
    "amount": <value>,
    "price": <value>
  },
}
```

Element name | Value | Description
--- | --- | ---
messageType	| "modification" | AIt’s a copy of messageType message property 
id | integer value (positive) | Unique id of an order
timestamp | long value (positive) | Timestamp telling indicating when an order was sent. <br>For two orders the one with the higher timestamp is the one which was sent later. <br>This applies across all brokers.
broker | String value | Identifier of a broker that sent the message
modifiedOrderId	| integer value	| ID of an order to be modified
details	| Sub-element | Sub-element containing details related to amount and price described below
amount | integer value | New amount of shares to buy or sell
price | integer value | New price limit associated with the order

Modification order example:

```json
{
  "messageType": "modification",
  "id": 3,
  "timestamp": 3,
  "broker": "b001",
  "modifiedOrderId": 1,
  "details": {
    "amount": 10,
    "price": 1.25
  }
}
```

The above order means: client of broker b001 wants to modify his order – order with id = 1 is now a buy order of 10 shares of abc for no more than 1.25 PLN.

## Cancellation order message (messageType = „cancel”)

This order cancels a buy or sell order in the order book. A buy or sell order that was already fully executed (all shares have been bought or sold) cannot be cancelled.
A cancellation order must be sent by the same broker that sent the related buy or sell order.

```json
{
  "messageType": "cancel",
  "id": <value>,
  "timestamp": <value>,
  "broker": <value>,
  "cancelledOrderId": <value>
}
```

Element name | Value | Description
--- | --- | ---
messageType	| "cancel" | AIt’s a copy of messageType message property 
id  | integer value (positive) | Unique id of an order
timestamp | long value (positive) | Timestamp telling when an order was sent. <br>For two orders the one with the higher timestamp is the one which was sent later. <br>This applies across all brokers.
broker | String value | Identifier of a broker that sent the message
cancelledOrderId | integer value | ID of an order to be cancelled

Example of a cancellation order:

```json
{
  "messageType": "cancel",
  "id": 4,
  "timestamp": 4,
  "broker": "b001",
  "cancelledOrderId": 2
}
```

The above order means: client of broker b001 wants to cancel his order with id = 2. Sell order with id = 2 will be removed from order book of xyz security.

## Shutdown notification message (messageType = „shutdownNotification”)

This message notifies the transaction engine that there will be no more messages from a given broker.

```json
{
  "messageType": "shutdownNotification"
}
```

When the transaction engine receives a shutdown notification from all brokers it is connected to, it should:

* close all its resources
* call the `processingDone()` method on the processingListener instance and pass the results

#  Order handling mechanism

Your transaction engine is expected to mimic a stock exchange behavior in a simplistic way. The orders are processed in continuous trading without fixing phases.
 
There are only four supported orders:

* buy order with limit – buys a given security (product) for no more than the given price;
* sell order with limit – sells a given security (product) for no less than the given price;
* cancellation order – cancels a buy or sell order which wasn’t executed or was executed only partially. In case of a partially executed order, the part that was not executed will be cancelled;
* modification order – modifies the buy or sell order which wasn’t executed or was executed only partially. In case of a partially executed order, the part that was not executed will be modified.;

The incoming orders should be handled instantly in the order of arrival (if in doubt you can use the message timestamp element to apply the order).
The outcome of order processing is represented by two entities: order books and transactions. Order books keeps active buy and sell orders for given security. 
Buy orders are sorted by descending price and then by the order of arrival. It means that the first buy order to execute is the one with the highest price. If buy orders have the same price, they will be executed in the order of arrival.
Sell orders are sorted by ascending price and then by the order of arrival. It means that the first sell order to execute is the one with the lowest price. If sell orders have the same price, they will be executed in the order of arrival.
Let’s take a look at two examples.

## Example 1
We start with an empty transaction list and order books.

1. broker 1 sends a sell order: 100 shares of ABC with a price limit of 100 PLN.
![example 1 step 1](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s1.png "example 1 step 1")
2. the order ends up in the order book for the ABC security.
![example 1 step 2](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s2.png "example 1 step 2")
3. broker 2 sends a sell order: 200 shares of ABC with a price limit of 90 PLN.
![example 1 step 3](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s3.png "example 1 step 3")
4. This order came as thea second one yet it has a lower price, so it will be placed in ABC`s order book as the first sell order.
![example 1 step 4](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s4.png "example 1 step 4")
5. broker 1 sends a sell order: 300 shares of ABC with a price limit of 95 PLN.
![example 1 step 5](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s5.png "example 1 step 5")
6. This has a lower price than the order with id = 1 and a higher price than the order with id = 2, so it is placed between the two.
![example 1 step 6](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s6.png "example 1 step 6")
7. broker 3 sends a buy order: 1000 shares of ABC with a price limit of 110 PLN.
![example 1 step 7](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s7.png "example 1 step 7")
8. The order is placed in ABC’s order book. 
![example 1 step 8](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s8.png "example 1 step 8")
9. The limits of first buy and first sell orders were matching, so the first transaction is recorded with price 90 and amount 200. The sell order was placed first, so the transaction price is dictated by its limit. The first sell order was fully executed, so it disappears from ABC’s order book. The buy order was executed partially, so it is updated.
![example 1 step 9](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s9.png "example 1 step 9")
10. The limits of first buy and first sell orders were matching again, so the second transaction is recorded with price 95 and amount 300. The sell order was placed first so the transaction price is dictated by its limit. The first sell order was fully executed, so it disappears from ABC’s order book. The buy order was executed partially again, so it is updated.
![example 1 step 10](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s10.png "example 1 step 10")
11. The limits of first buy and first sell orders were matching again so the third transaction is recorded with price 100 and amount 100. The sell order was placed first so the transaction price is dictated by its limit. The first sell order was fully executed, so it disappears from ABC’s order book. The buy order was executed partially again, so it is updated.
![example 1 step 11](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s11.png "example 1 step 11")
12. broker 1 sends a sell order: 100 shares of ABC with a price limit of 100 PLN.
![example 1 step 12](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s12.png "example 1 step 12")
13. The order is placed in ABC’s order book. 
![example 1 step 13](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s13.png "example 1 step 13")
14. The limits of buy and sell orders were matching, so the forth transaction is recorded with price 110 and amount 100. This time, the buy order was placed first, so the transaction price is dictated by its limit. The first sell order was fully executed, so it disappears from ABC’s order book. The buy order was executed partially again, so it is updated.
![example 1 step 14](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s14.png "example 1 step 14")
15. broker 3 sends a cancellation order of order with id=4 (buy order).
![example 1 step 15](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s15.png "example 1 step 15")
16. The order with id=4 (buy order) was partially executed, so it means that it can be cancelled. None of the resultant transactions will be rollback. The only result of cancellation of the buy order will be its disappearance from ABC’s book.
![example 1 step 16](https://bytebucket.org/digitalbanking/contest2016/raw/master/images/e1s16.png "example 1 step 16")
 
The example ends up with four transactions and an empty order book.

## Example 2
The below example won’t will not be discussed as minutely as the previous one.
Let’s consider an opposite case, where there are three relatively small buy orders and one relatively large sell order.
The order flow is as follows:

1. buy 100 shares of ABC with a price limit of 100 PLN
2. buy 200 shares of ABC with a price limit of 90 PLN
3. buy 300 shares of ABC with a price limit of 95 PLN
4. sell 500 shares of ABC with a price limit of 90 PLN

The resulting transactions would be:

1. id=ABC-1, prod: ABC, amount: 100, price: 100 PLN
2. id=ABC-2, prod: ABC, amount: 300, price: 95 PLN
3. id=ABC-3, prod: ABC, amount: 100, price: 90 PLN

The order book for ABC would be:

1. buy order for 100 shares with price limit of 90 PLN

Results
When all the brokers send shutdown notification messages and all the order messages have been processed, your solution should call the processingDone method and provide a SolutionResults instance, which can be built using SolutionResults.builder():

```java
processingListener.processingDone(
   SolutionResult.builder()
      .transactions(transactions) // transactions is a Set<Transaction>
      .orderBooks(orderBooks) // orderBooks is a Set<OrderBook> 
      .build()
);
```

After calling `processingDone`, the verification process will start.

# Verification
There are two types of verification tests:

* functional tests
There are 48 functional tests,  and they are executed by invoking the mvn test command in the project’s root folder. The purpose of these tests is to make sure that certain order flows are handled correctly by your solution. 
* performance tests
There is one performance test that is executed by invoking the mvn performanceTest command in the project root folder. The purpose of of these teststhis test is to make sure that your solution is able to handle a large load of orders.

Before each test, a new instance of your Exchange implementation is created and initialized. After each test, ActiveMQ queues are purged.
The presence of verification tests does not mean that unit tests should be neglected.